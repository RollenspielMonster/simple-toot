# Simple Toot

This is a Bash script designed to automate the process of sending welcoming messages to new users on a Mastodon instance. It utilizes the Mastodon API to send these messages.

## Prerequisites

- **Bash**: Ensure you have Bash installed on your system.
- **Curl**: Used for making HTTP requests.
- **jq**: A lightweight and flexible command-line JSON processor. You'll need this to parse JSON responses.
- **Access to a Mastodon Instance**: You'll need access to a Mastodon instance to utilize this script.

## Setup

1. Clone the repository:

    ```bash
    git clone https://codeberg.org/RollenspielMonster/simple-toot.git
    ```

2. Navigate to the cloned directory:

    ```bash
    cd simple-toot
    ```

3. Make the script executable:

    ```bash
    chmod +x toot.sh
    ```

4. Update the configuration variables in the script:

   - `TOOL_NAME`: The name of your tool.
   - `SERVER`: The Mastodon instance's domain.
   - `CONFIG_PATH`: The directory where configuration files will be stored.

## Usage

1. Run the script:

    ```bash
    ./toot.sh
    ```

2. Follow the on-screen instructions to authorize the tool to use your Mastodon account.

3. The script will send welcome messages to new users on your Mastodon instance.

### Running with Systemd Timer

To schedule the execution of the script using a systemd timer to run every 5 minutes, follow these steps:

1. Create a Systemd Service File:

    Create a file named `simple-toot.service` in the `/etc/systemd/system/` directory with the following content:

    ```plaintext
    [Unit]
    Description=Mastodon simple-toot service

    [Service]
    Type=oneshot
    User=root
    WorkingDirectory=/path/to
    ExecStart=/path/to/toot.sh

    [Install]
    WantedBy=multi-user.target
    ```

    Replace `/path/to/toot.sh` with the actual path to your script.

2. Create a Systemd Timer File:

    Create a file named `simple-toot.timer` in the `/etc/systemd/system/` directory with the following content:

    ```plaintext
    [Unit]
    Description=Run Simple Toot Script every 5 minutes

    [Timer]
    OnCalendar=*:0/5
    Unit=simple-toot.service

    [Install]
    WantedBy=timers.target
    ```

3. Enable and Start the Timer:

    Run the following commands to enable and start the timer:

    ```bash
    sudo systemctl enable simple-toot.timer
    sudo systemctl start simple-toot.timer
    ```

    This will start executing your script every 5 minutes.

4. Verify Timer Status:

    To check the status of the timer, use:

    ```bash
    sudo systemctl status simple-toot.timer
    ```

    This command will show whether the timer is active and when it is next scheduled to run.

### Note

- Ensure to replace `/path/to` with the actual path to your script in both the service and timer files.
- Adjust the `OnCalendar` parameter in the timer file if you wish to change the interval at which the script runs.

## Important Notes

- Ensure you have read and understand the Mastodon instance's policies regarding automated actions and spam prevention.
- It's recommended to run this script in a controlled environment to avoid unexpected behavior.
- This script assumes you have administrative privileges on the Mastodon instance.
- Make sure to review and adjust visibility settings (`-F "visibility=unlisted"`) in the `send_toot` function as per your instance's policies.

## Disclaimer

This script is provided as-is without any guarantees. Use it at your own risk.

## License

This project is licensed under the [Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License](LICENSE).