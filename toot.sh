#!/usr/bin/env bash

# Enable strict mode
set -o errexit -o pipefail -o nounset

# Set variables
readonly TOOL_NAME="simple-toot"
readonly SERVER="rollenspiel.social"
readonly CONFIG_PATH=".config"
readonly ACCESS_TOKEN_FILE="${CONFIG_PATH}/access_token"
readonly APPLICATION_TOKEN_FILE="${CONFIG_PATH}/application_token"
readonly CLIENT_ID_FILE="${CONFIG_PATH}/client_id"
readonly CLIENT_SECRET_FILE="${CONFIG_PATH}/client_secret"
readonly LATEST_USERNAME_FILE="latest_username.txt"
readonly -a DEPENDENCIES=("curl" "jq")

# Create directory if it doesn't exist
mkdir -p "${CONFIG_PATH}"

# Function: handle_error
# Purpose: Handles errors by printing an error message to stderr and exiting with a status code of 1.
# Parameters:
#   $1 - The error message to be printed.
# Return value: None
handle_error() {
  local error_text="${1}"
    printf 'Error: %s\n' "${error_text}" >&2
    exit 1
}

# Function: check_command
# Purpose: Checks if a given command is available on the system.
# Parameters:
#   $1 - The name of the command to check
# Return value: None
check_command() {
  local command_name="${1}"
  if ! command -v "${command_name}" >/dev/null 2>&1; then
    handle_error "${command_name} is not installed."
  fi
}

# Function: check_dependency
# Purpose: Checks and verifies dependencies required for script execution.
# Parameters: None
# Return value: None
check_dependency() {
  if [[ -z "${DEPENDENCIES:-}" ]]; then
    handle_error "Missing dependencies"
  fi
  for dependency in "${DEPENDENCIES[@]}"; do
    check_command "${dependency}"
  done
}

# Function: get_application_token
# Purpose: Obtains the user token required for authentication.
# Parameters: None
# Return value: None
get_application_token() {
    if [[ -z "${APPLICATION_TOKEN_FILE}" || ! -s "${APPLICATION_TOKEN_FILE}" ]]; then
        echo "Please visit the following URL to create an application with 'admin:read:accounts' scope:"
        echo "https://${SERVER}/settings/applications/new"
        read -rp "Enter the application token: " app_token
        if [[ -z "$app_token" ]]; then
            handle_error "No input provided, exiting."
        fi
        echo "$app_token" >"${APPLICATION_TOKEN_FILE}"
        if [[ ! -f "${APPLICATION_TOKEN_FILE}" ]]; then
            handle_error "Failed to create ${APPLICATION_TOKEN_FILE}"
        fi
        chmod 600 "${APPLICATION_TOKEN_FILE}"
    fi
}

# Function: get_client_id_and_secret
# Purpose: Retrieves the client ID and client secret for OAuth.
# Parameters: None
# Return value: None
get_client_id_and_secret() {
    if [[ -z "${CLIENT_ID_FILE}" ]] || [[ ! -s "${CLIENT_ID_FILE}" || -z "${CLIENT_SECRET_FILE}" ]] || [[ ! -s "${CLIENT_SECRET_FILE}" ]]; then
        local app_response
        app_response=$(curl -s -X POST \
            -F "client_name=${TOOL_NAME}" \
            -F "redirect_uris=urn:ietf:wg:oauth:2.0:oob" \
            -F "scopes=write" \
            -F "website=https://${SERVER}" \
            "https://${SERVER}/api/v1/apps")

        if [[ -z "${app_response}" ]]; then
            handle_error "Failed to obtain app response, exiting."
        fi

        local client_id
        client_id=$(jq -r '.client_id' <<<"$app_response")
        if [[ -z "${client_id}" ]]; then
            handle_error "Failed to obtain client ID, exiting."
        fi

        local client_secret
        client_secret=$(jq -r '.client_secret' <<<"$app_response")
        if [[ -z "${client_secret}" ]]; then
            handle_error "Failed to obtain client secret, exiting."
        fi

        echo "${client_id}" >"${CLIENT_ID_FILE}"
        echo "${client_secret}" >"${CLIENT_SECRET_FILE}"
        chmod 600 "${CLIENT_ID_FILE}" "${CLIENT_SECRET_FILE}"

        request_authorization
    fi
}

# Function: request_authorization
# Purpose: Requests user authorization for OAuth.
# Parameters: None
# Return value: None
request_authorization() {
    if [[ -z "$(cat "${CLIENT_ID_FILE}")" ]] || [[ -z "$(cat "${CLIENT_SECRET_FILE}")" ]]; then
        handle_error "${TOOL_NAME} is not yet authorized to use your account. Delete ${CLIENT_ID_FILE} and ${CLIENT_SECRET_FILE} and try again."
    fi

    echo "Please visit the following URL in your browser to authorize ${TOOL_NAME} to toot as your user:"
    echo "https://${SERVER}/oauth/authorize?response_type=code&client_id=$(cat "${CLIENT_ID_FILE}")&redirect_uri=urn:ietf:wg:oauth:2.0:oob&scope=write"

    read -rp "Enter the authorization code: " AUTH_CODE
    if [[ -z "${AUTH_CODE}" ]]; then
        handle_error "No input provided."
    fi

    AUTH_RESPONSE=$(curl -s -X POST \
        -F "client_id=$(cat "${CLIENT_ID_FILE}")" \
        -F "client_secret=$(cat "${CLIENT_SECRET_FILE}")" \
        -F "scope=write" \
        -F "grant_type=authorization_code" \
        -F "code=${AUTH_CODE}" \
        -F "redirect_uri=urn:ietf:wg:oauth:2.0:oob" \
        "https://${SERVER}/oauth/token")

    if [[ -z "$(jq -r '.access_token' <<<"${AUTH_RESPONSE}")" ]]; then
        handle_error "Failed to obtain access token."
    fi

    jq -r '.access_token' <<<"${AUTH_RESPONSE}" >"${ACCESS_TOKEN_FILE}"
    chmod 600 "${ACCESS_TOKEN_FILE}"
}

# Function: send_toot
# Purpose: Sends a toot with the given message.
# Parameters: None
# Return value: None
send_toot() {
    if [[ -z "$(cat "${ACCESS_TOKEN_FILE}")" ]]; then
        handle_error "${TOOL_NAME} is not yet authorized to use your account. Delete ${ACCESS_TOKEN_FILE} and try again."
    fi
    local ACCESS_TOKEN
    ACCESS_TOKEN=$(cat "${ACCESS_TOKEN_FILE}")
    local TEXT=":rm_love: willkommen auf unserer Instanz und im #fediverse

${users_string}

#neuhier

Falls ihr Fragen habt einfach raus damit, im #fediverse sind viele nette Menschen, die euch gerne helfen werden.
Ansonsten könnt ihr euch gerne einmal in unserem Wiki umsehen: https://rollenspiel.wiki/shelves/rollenspielmonster

Neuigkeiten findet ihr unter @rollenspielmonster oder für #RollenspielMonster auf @rm_news@rollenspiel.forum."

    local RESPONSE
    RESPONSE=$(curl -s -X POST \
        -H "Authorization: Bearer ${ACCESS_TOKEN}" \
        -F "status=${TEXT}" \
        -F "visibility=unlisted" \
        "https://${SERVER}/api/v1/statuses")
    if [[ -z "$(jq -r '.id' <<<"${RESPONSE}")" ]]; then
        handle_error "Failed to send toot."
    fi
}

# Function: process_users
# Purpose: Processes new users and sends toots for each new user.
# Parameters: None
# Return value: None
process_users() {
    local latest_usernames=()
    if [[ -f "${LATEST_USERNAME_FILE}" ]]; then
        readarray -t latest_usernames < "${LATEST_USERNAME_FILE}"
    fi

    local current_usernames
    current_usernames=$(curl -s -H "Authorization: Bearer $(cat "${APPLICATION_TOKEN_FILE}")" "https://${SERVER}/api/v2/admin/accounts?origin=local&status=active" | jq -r '.[].username')

    local new_users=()
    if [[ -z ${latest_usernames[*]} ]]; then
        for username in ${current_usernames}; do
            new_users+=("${username}")
        done
    else
        for username in ${current_usernames}; do
            local found=false
            for latest_username in "${latest_usernames[@]}"; do
                if [[ "${latest_username}" = "${username}" ]]; then
                    found=true
                    break
                fi
            done
            if ! ${found}; then
                new_users+=("${username}")
            fi
        done
    fi

    local users_string=""
    for user in "${new_users[@]}"; do
        if [[ ${#users_string} -gt 0 ]]; then
            users_string+=", @${user}"
        else
            users_string+="@${user}"
        fi

        if [[ ${#users_string} -gt 132 ]]; then
            send_toot
            users_string=""
        fi
    done

    if [[ ${#users_string} -gt 0 ]]; then
        send_toot
    fi
    printf "%s\n" "${current_usernames[@]}" > "${LATEST_USERNAME_FILE}"
}

check_dependency
get_client_id_and_secret
get_application_token
process_users

